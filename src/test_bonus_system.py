from bonus_system import calculateBonuses

def test_invalid_cases():
    assert calculateBonuses("1000", 1000) == 0
    assert calculateBonuses("", 1000) == 0
    assert calculateBonuses("", 250000) == 0
    assert calculateBonuses("", 100000) == 0
    assert calculateBonuses("ad", 1000) == 0
    assert calculateBonuses("adsad", 1000) == 0
    assert calculateBonuses("oo", 1000) == 0

def test_cases():
    assert calculateBonuses("Standard", 1000) == 0.5
    assert calculateBonuses("Diamond", 50000) == 0.2 * 2
    assert calculateBonuses("Standard", 10000) == 0.5 * 1.5
    assert calculateBonuses("Standard", 250000) == 0.5 * 2.5
    assert calculateBonuses("Premium", 1000) == 0.1
    assert calculateBonuses("Diamond", 5000) == 0.2
    assert calculateBonuses("Premium", 25000) == 0.1 * 1.5
    assert calculateBonuses("Premium", 50000) == 0.1 * 2
    assert calculateBonuses("Premium", 100000) == 0.1 * 2.5
    assert calculateBonuses("Diamond", 10000) == 0.2 * 1.5
    assert calculateBonuses("Standard", 50000) == 0.5 * 2
    assert calculateBonuses("Diamond", 70000) == 0.2 * 2
    assert calculateBonuses("Diamond", 250000) == 0.2 * 2.5
